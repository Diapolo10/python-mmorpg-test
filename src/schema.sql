DROP TABLE IF EXISTS users;
DROP TABLE IF EXISTS accounts;
DROP TABLE IF EXISTS news;
DROP TABLE IF EXISTS chat;

CREATE TABLE users (
    /* Users are what people use to log in to their accounts */
    id INTEGER PRIMARY KEY,
    email TEXT UNIQUE NOT NULL,     /* Needed for password recovery and to inform the user; can be used for logging in */
    username TEXT NOT NULL,         /* Used to refer to the user by name */
    password TEXT NOT NULL,         /* Stores the hash of the password used to login as this specific user */
    pw_salt TEXT NOT NULL,
    created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    
    type INTEGER NOT NULL CHECK (
        type IN (
            0, --"player"
            1, --"player_moderator"
            2, --"moderator"
            3, --"admin"
            4  --"super_admin"
        )
    ) DEFAULT 0,

    status INTEGER NOT NULL CHECK (
        status IN (
            0, --"active"
            1, --"disabled"
            2, --"banned"
            3  --"permanently_banned"
        )
    ) DEFAULT 0,
    
    email_verified BOOLEAN NOT NULL CHECK (
        email_verified IN (0,1)
    ) DEFAULT 0,
    
    profile_picture TEXT /* Link to the user's profile picture */
);

CREATE TABLE accounts (
    /* Accounts represent the users' in-game avatars, each user can have multiple accounts */
    id INTEGER PRIMARY KEY,
    owner_id INTEGER NOT NULL,      /* The ID of the user that owns this account */
    created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    account_name TEXT NOT NULL,

    /* Account stats in EXP */
    attack INTEGER NOT NULL DEFAULT 0,
    hitpoints INTEGER NOT NULL DEFAULT 0,
    mining INTEGER NOT NULL DEFAULT 0,
    strength INTEGER NOT NULL DEFAULT 0,
    agility INTEGER NOT NULL DEFAULT 0,
    smithing INTEGER NOT NULL DEFAULT 0,
    defence INTEGER NOT NULL DEFAULT 0,
    herblore INTEGER NOT NULL DEFAULT 0,
    fishing INTEGER NOT NULL DEFAULT 0,
    ranged INTEGER NOT NULL DEFAULT 0,
    thieving INTEGER NOT NULL DEFAULT 0,
    cooking INTEGER NOT NULL DEFAULT 0,
    prayer INTEGER NOT NULL DEFAULT 0,
    crafting INTEGER NOT NULL DEFAULT 0,
    firemaking INTEGER NOT NULL DEFAULT 0,
    magic INTEGER NOT NULL DEFAULT 0,
    fletching INTEGER NOT NULL DEFAULT 0,
    woodcutting INTEGER NOT NULL DEFAULT 0,
    runecrafting INTEGER NOT NULL DEFAULT 0,
    slayer INTEGER NOT NULL DEFAULT 0,
    farming INTEGER NOT NULL DEFAULT 0,
    construction INTEGER NOT NULL DEFAULT 0,
    hunter INTEGER NOT NULL DEFAULT 0,
    summoning INTEGER NOT NULL DEFAULT 0,
    dungeoneering INTEGER NOT NULL DEFAULT 0,
    divination INTEGER NOT NULL DEFAULT 0,
    invention INTEGER NOT NULL DEFAULT 0,

    FOREIGN KEY (owner_id) REFERENCES users (id)
);

CREATE TABLE chat (
    /* Logs all chat in the game */
    id INTEGER PRIMARY KEY,
    owner_id INTEGER NOT NULL,
    created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    message TEXT NOT NULL,

    FOREIGN KEY (owner_id) REFERENCES accounts (id)
);

CREATE TABLE news (
    /* Contains all news posts available from the front page */
    id INTEGER PRIMARY KEY,
    author_id INTEGER NOT NULL,
    author_name TEXT NOT NULL,
    created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    last_edited TIMESTAMP,
    title TEXT NOT NULL,
    heading TEXT NOT NULL,
    body TEXT NOT NULL,

    FOREIGN KEY (author_id) REFERENCES users (id)
);