from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for
)
from werkzeug.exceptions import abort

from src.auth import login_required
from src.db import get_db

bp = Blueprint('news', __name__)

def get_post(id, check_author=True):
    post = get_db().execute(
        'SELECT n.id, title, heading, body, n.created, author_id, author_name'
        ' FROM news n JOIN users u ON n.author_id = u.id'
        ' WHERE n.id = ?',
        (id,)
    ).fetchone()

    if post is None:
        abort(404, f"Post id {id} doesn't exist.")

    if check_author and post['author_id'] != g.user['id']:
        abort(403)

    return post


@bp.route('/')
def index():
    db = get_db()
    posts = db.execute(
        'SELECT n.id, title, heading, body, n.created, author_id, author_name'
        ' FROM news n JOIN users u ON n.author_id = u.id'
        ' ORDER BY n.created DESC'
    ).fetchall()

    return render_template('news/index.html', posts=posts)


@bp.route('/create', methods=('GET', 'POST'))
@login_required
def create():
    if request.method == 'POST':
        title = request.form['title']
        heading = request.form['heading']
        body = request.form['body']
        error = None

        if not title:
            error = "Title is required."

        if error is not None:
            flash(error)
        else:
            db = get_db()
            db.execute(
                'INSERT INTO news (title, heading, body, author_id, author_name)'
                ' VALUES (?, ?, ?, ?, ?)',
                (title, heading, body, g.user['id'], g.user['username'])
            )
            db.commit()
            return redirect(url_for('news.index'))

    return render_template('news/create.html')


@bp.route('/<int:id>/', methods=('GET', 'POST'))
def article(id):
    post = get_post(id, check_author=False)

    return render_template('news/article.html', post=post)


@bp.route('/<int:id>/update', methods=('GET', 'POST'))
@login_required
def update(id):
    post = get_post(id)

    if request.method == 'POST':
        title = request.form['title']
        heading = request.form['heading']
        body = request.form['body']
        error = None

        if not title:
            error = "Title is required."

        if error is not None:
            flash(error)
        else:
            db = get_db()
            db.execute(
                'UPDATE news SET title = ?, heading = ?, body = ?'
                ' WHERE id = ?',
                (title, heading, body, id)
            )
            db.commit()
            return redirect(url_for('news.index'))

    return render_template('news/update.html', post=post)
