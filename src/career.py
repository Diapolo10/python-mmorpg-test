from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for
)
from werkzeug.exceptions import abort

from src.auth import login_required, verified_email_required
from src.db import get_db

bp = Blueprint('career', __name__, url_prefix='/career')

@bp.route('/')
@login_required
@verified_email_required
def index():
    flash("The career path you seek is not yet to be found.")
    return redirect(url_for('index'))
    return render_template('forums/index.html')