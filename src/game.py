from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for
)
from werkzeug.exceptions import abort

from src.auth import login_required, verified_email_required
from src.db import get_db

bp = Blueprint('game', __name__, url_prefix='/game')

@bp.route('/', methods=('GET', 'POST'))
@login_required
@verified_email_required
def index():
    db = get_db()
    accounts = db.execute(
        'SELECT * FROM accounts WHERE owner_id = ?'
        ' ORDER BY created DESC',
        (g.user['id'],)
    ).fetchall()

    return render_template('game/index.html', accounts=accounts)

@bp.route('/new_account', methods=('GET', 'POST'))
@login_required
@verified_email_required
def new_account():
    if request.method == 'POST':
        account_name = request.form['account_name']
        error = None

        if not account_name.strip():
            error = "Account name is required."

        if error is not None:
            flash(error)
        else:
            db = get_db()
            db.execute(
                'INSERT INTO accounts (account_name, owner_id)'
                ' VALUES (?, ?)',
                (account_name, g.user['id'])
            )
            db.commit()
            return redirect(url_for('game.index'))

    return render_template('game/new_account.html')


@bp.route('/profile/<int:id>/', methods=('GET', 'POST'))
@login_required
@verified_email_required
def account(id):
    db = get_db()

    account = db.execute(
        'SELECT * FROM accounts WHERE id = ?',
        (id,)
    ).fetchone()

    return render_template('game/account.html', account=account)
