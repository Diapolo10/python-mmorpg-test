from pathlib import Path

from flask import Flask


def create_app(test_config=None):
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        SECRET_KEY='dev',
        DATABASE=str(Path(app.instance_path) / 'mmo.sqlite'),
    )

    if test_config is None:
        # Load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py', silent=True)
    else:
        # Load the config if passed in
        app.config.from_mapping(test_config)

    app.jinja_env.trim_blocks = True
    app.jinja_env.lstrip_blocks = True

    from . import db
    db.init_app(app)

    from . import auth
    app.register_blueprint(auth.bp)

    from . import news
    app.register_blueprint(news.bp)
    app.add_url_rule('/', endpoint='index')

    from . import game
    app.register_blueprint(game.bp)

    from . import forums
    app.register_blueprint(forums.bp)

    from . import support
    app.register_blueprint(support.bp)

    from . import career
    app.register_blueprint(career.bp)


    @app.route('/hello')
    def hello():
        return "Hello, World!"


    return app