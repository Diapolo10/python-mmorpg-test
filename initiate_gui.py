import os

from pyfladesk import init_gui

from src import create_app

os.environ['FLASK_DEBUG'] = False
os.environ['FLASK_APP'] = "src"
os.environ['FLASK_ENV'] = "development"

app = create_app()
if __name__ == '__main__':
    init_gui(
        app,
        window_title="RunesCape",
        port=5000,
        width=640,
        height=480
    )